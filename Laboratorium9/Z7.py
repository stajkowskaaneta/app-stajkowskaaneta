from tkinter import *
from tkinter import font
import random
import sys
from tkinter import messagebox

def main():
    root = Tk()
    default_font = font.nametofont("TkDefaultFont")
    default_font.configure(size=random.randint(12,15))
    root.option_add("*Font", default_font)
    root.title("Imie")
    root.geometry("480x300")
    app = Application(root)
    root.configure(background = random.sample(["black", "green", "red"],  1));
    root.mainloop()
          
class Application(Frame):

    def __init__(self, master):
        super().__init__(master)  
        self.grid()
        self.create_widgets()

    def create_widgets(self):

        self.inst_lbl = Label(self, text = "Imie: ")
        self.inst_lbl.grid(row = 0, column = 0, columnspan = 2, sticky = W)
        self.pw_ent = Entry(self)
        self.pw_ent.grid(row = 0, column = 1, sticky = W)
        
        self.pw_lbl1 = Label(self, text = "Nazwisko: ")
        self.pw_lbl1.grid(row = 1, column = 0, sticky = W)
        self.pw_ent1 = Entry(self)
        self.pw_ent1.grid(row = 1, column = 1, sticky = W)
        
        self.pw_lbl2 = Label(self, text = "Pesel: ")
        self.pw_lbl2.grid(row = 2, column = 0, sticky = W)
        self.pw_ent2 = Entry(self)
        self.pw_ent2.grid(row = 2, column = 1, sticky = W)

        self.pw_lbl3 = Label(self, text = "Numer konta: ")
        self.pw_lbl3.grid(row = 3, column = 0, sticky = W)
        self.pw_ent3 = Entry(self)
        self.pw_ent3.grid(row = 3, column = 1, sticky = W)
        
        self.pw_lbl4 = Label(self, text = "Stan konta: ")
        self.pw_lbl4.grid(row = 4, column = 0, sticky = W)
        self.pw_ent4 = Entry(self)
        self.pw_ent4.grid(row = 4, column = 1, sticky = W)
        
        self.submit_bttn = Button(self, text = "Zapisz", command = self.reveal)
        self.submit_bttn.grid(row = 5, column = 0, sticky = W)

        #self.secret_txt = Text(self, width = 35, height = 5, wrap = WORD)
        #self.secret_txt.grid(row = 6, column = 0, columnspan = 2, sticky = W)

    def reveal(self):
        try:
            if len(self.pw_ent2.get()) != 9:
                raise ValueError
            plik = open("Dane.txt", "w")
            plik.write("Nazwisko: " + self.pw_ent.get() + "\n")
            plik.write("Imie: " + self.pw_ent1.get()+ "\n")
            plik.write("PESEL: " + str(int(self.pw_ent2.get()))+ "\n")
            plik.write("Numer konta: " + self.pw_ent3.get()+ "\n")
            plik.write("Stan konta: " + self.pw_ent4.get()+ "\n")

        except:
            messagebox.showinfo( "blad", "Cos poszlo nie tak\n Sprawdz poprawnosc danych")
            sys.exit()


if __name__ == "__main__":
    main()


