#!/usr/bin/env python3

from tkinter import Tk, W, E, font
from tkinter.ttk import Frame, Button, Style
from tkinter.ttk import Entry
import sys

class Example(Frame):
    def __init__(self, parent):
        self.i = 0
        Frame.__init__(self, parent)   
        self.parent = parent
        self.initUI()

    

        
    def initUI(self):
      
        self.parent.title("Calculator")
        
        Style().configure("TButton", padding=(0, 5, 0, 5), font='sans 16')
        
        self.columnconfigure(0, pad=3)
        self.columnconfigure(1, pad=3)
        self.columnconfigure(2, pad=3)
        self.columnconfigure(3, pad=3)
        
        self.rowconfigure(0, pad=3)
        self.rowconfigure(1, pad=3)
        self.rowconfigure(2, pad=3)
        self.rowconfigure(3, pad=3)
        self.rowconfigure(4, pad=3)
        
        self.entry = Entry(self)
        self.entry.grid(row=0, columnspan=4, sticky=E+W)
        
        cls = Button(self, text="Cls", command = lambda: self.cl())
        cls.grid(row=1, column=0)
        bck = Button(self, text="Back", command = lambda: self.bk())
        bck.grid(row=1, column=1)
        lbl = Button(self)
        lbl.grid(row=1, column=2)    
        clo = Button(self, text="Close", command = lambda: sys.exit())
        clo.grid(row=1, column=3)        
        sev = Button(self, text="7", command = lambda: self.wypisz(7))
        sev.grid(row=2, column=0)        
        eig = Button(self, text="8", command = lambda : self.wypisz(8))
        eig.grid(row=2, column=1)         
        nin = Button(self, text="9", command = lambda: self.wypisz(9))
        nin.grid(row=2, column=2) 
        div = Button(self, text="/", command = lambda: self.wypisz("/"))
        div.grid(row=2, column=3) 
        
        fou = Button(self, text="4", command = lambda: self.wypisz(4))
        fou.grid(row=3, column=0)        
        fiv = Button(self, text="5", command = lambda: self.wypisz(5))
        fiv.grid(row=3, column=1)         
        six = Button(self, text="6", command = lambda: self.wypisz(6))
        six.grid(row=3, column=2) 
        mul = Button(self, text="*", command = lambda: self.wypisz("*"))
        mul.grid(row=3, column=3)    
        
        one = Button(self, text="1", command = lambda: self.wypisz(1))
        one.grid(row=4, column=0)        
        two = Button(self, text="2", command = lambda: self.wypisz(2))
        two.grid(row=4, column=1)         
        thr = Button(self, text="3", command = lambda: self.wypisz(3))
        thr.grid(row=4, column=2) 
        mns = Button(self, text="-", command = lambda: self.wypisz("-"))
        mns.grid(row=4, column=3)         
        
        zer = Button(self, text="0", command = lambda: self.wypisz(0))
        zer.grid(row=5, column=0)        
        dot = Button(self, text=".", command = lambda: self.wypisz("."))
        dot.grid(row=5, column=1)         
        equ = Button(self, text="=", command = lambda: self.oblicz())
        equ.grid(row=5, column=2) 
        pls = Button(self, text="+", command = lambda: self.wypisz("+"))
        pls.grid(row=5, column=3)
        
        self.pack()
        
    def wypisz(self, v):
        self.entry.insert(self.i, v)   
        self.i += 1
    
    def oblicz(self):
        wynik = self.entry.get()
        wynik = eval(wynik)
        self.entry.delete(0, 100)
        self.entry.insert(0, wynik)
        self.i = len(str(wynik))
    
    def cl(self):
        self.entry.delete(0, 100)
        self.i = 0
        
    def bk(self):
        self.entry.delete(self.i-1)
        self.i -= 1
        if self.i < 0:
            self.i = 0
        
def main():
    root = Tk()
    app = Example(root)
    root.mainloop()  

if __name__ == '__main__':
    main()

