from tkinter import *
from tkinter import font
import sys
from tkinter import messagebox

class Application(Frame):

    def __init__(self, master):
        super().__init__(master)  
        self.grid()
        self.create_widgets()

    def create_widgets(self):

        self.inst_lbl = Label(self, text = "Zamiana liczb arabskich na rzymskie!")
        self.inst_lbl.grid(row = 0, column = 0, columnspan = 2, sticky = W)
     
        self.pw_lbl = Label(self, text = "Liczba: ")
        self.pw_lbl.grid(row = 1, column = 0, sticky = W)

        self.pw_ent = Entry(self)
        self.pw_ent.grid(row = 1, column = 1, sticky = W)


        self.submit_bttn = Button(self, text = "Zamien!", command = self.reveal)
        self.submit_bttn.grid(row = 4, column = 0, sticky = W)

        self.secret_txt = Text(self, width = 35, height = 5, wrap = WORD)
        self.secret_txt.grid(row = 5, column = 0, columnspan = 2, sticky = W)

    def reveal(self):
        liczba = int(self.pw_ent.get())
        message = str(self.write_roman(liczba))           
            
        self.secret_txt.delete(0.0, END)
        self.secret_txt.insert(0.0, message)
        
    def write_roman(self, number):
        if number > 3999 or number < 1:
            messagebox.showinfo( "blad", "liczba musi byc z przedzialu <1,3999>")
            sys.exit()
        ROMAN_NUMERAL_TABLE = [
            ("M", 1000), ("CM", 900), ("D", 500),
            ("CD", 400), ("C", 100),  ("XC", 90),
            ("L", 50),   ("XL", 40),  ("X", 10),
            ("IX", 9),   ("V", 5),    ("IV", 4),
            ("I", 1)
        ]
        roman_numerals = []
        for numeral, value in ROMAN_NUMERAL_TABLE:
            while value <= number:
                number -= value
                roman_numerals.append(numeral)

        return ''.join(roman_numerals)

def main():
    root = Tk()
    default_font = font.nametofont("TkDefaultFont")
    default_font.configure(size=16)
    root.option_add("*Font", default_font)
    root.title("Suma")
    root.geometry("480x300")
    app = Application(root)
    root.mainloop()

main()
