import random  
from tkinter import *
from tkinter import font

def main():
    root = Tk()
    default_font = font.nametofont("TkDefaultFont")
    default_font.configure(size=16)
    root.option_add("*Font", default_font)
    root.title("Imie")
    root.geometry("480x300")
    app = Application(root)
    root.mainloop()
          
class Application(Frame):

    def __init__(self, master):
        super().__init__(master)  
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.number = int(random.randint(1, 1001))
        self.inst_lbl = Label(self, text = "Zgaduje liczbe!!")
        self.inst_lbl.grid(row = 0, column = 0, columnspan = 2, sticky = W)
     
        self.pw_lbl = Label(self, text = "Podaj liczbe z zakresu <1,1000>: ")
        self.pw_lbl.grid(row = 1, column = 0, sticky = W)
    
        self.pw_ent = Entry(self)
        self.pw_ent.grid(row = 1, column = 1, sticky = W)

        self.submit_bttn = Button(self, text = "Sprawdz czy to ta sama liczba", command = self.reveal)
        self.submit_bttn.grid(row = 2, column = 0, sticky = W)

        self.secret_txt = Text(self, width = 35, height = 5, wrap = WORD)
        self.secret_txt.grid(row = 3, column = 0, columnspan = 2, sticky = W)

    def reveal(self):
        contents = int(self.pw_ent.get())
        if contents == self.number:
            message = "Zgadles!!!"           
        elif contents > self.number:
            message = "mniej!"
        elif contents < self.number:
            message = "wiecej!"
        self.secret_txt.delete(0.0, END)
        self.secret_txt.insert(0.0, message)

if __name__ == "__main__":
    main()



