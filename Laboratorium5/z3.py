
class Samochod:

	def __init__(self,name,osob,typLad,masaLad):
		self.name = name
		self.osob = osob
		self.typLad = typLad
		self.masaLad = masaLad
	
	def Nazwa(self):
		print("Ten samochod jest firmy " + self.name)

	def Pomiesci(self):
		print("Ten samochod pomiesci " + str(self.osob) + " osob")

class Osobowy(Samochod):

	def Zajete(self,ile):
		self.osob += ile

	def Wolne(self,ile):
		self.osob -= ile

	def Wypisz(self):
		print(self.osob)


class Ciezarowy(Samochod):

	def Typ(self, typ):
		self.typLad = typ

	def WypiszTyp(self):
		print("Typ ladunku to " + self.typLad)

	def DodMas(self, ile):
		self.masaLad += ile

	def UsMas(self, ile):
		self.masaLad -= ile

	def WypiszMas(self):
		print(self.masaLad)

def main():
	A = Samochod("Reno", 2, "Drewno", 500)
	Samochod.Nazwa(A)
	Samochod.Pomiesci(A)
	Osobowy.Wypisz(A)
	Osobowy.Zajete(A,1)
	Osobowy.Wypisz(A)
	Osobowy.Wolne(A,2)
	Osobowy.Wypisz(A)

	Ciezarowy.WypiszTyp(A)
	Ciezarowy.Typ(A,"metal")
	Ciezarowy.WypiszTyp(A)
	Ciezarowy.WypiszMas(A)
	Ciezarowy.DodMas(A,500)
	Ciezarowy.WypiszMas(A)
	Ciezarowy.UsMas(A,700)
	Ciezarowy.WypiszMas(A)



main()

