
class Czlowiek:
	
	def __init__(self,imie, nazwisko, plec, wiek, wzrost):
		self.imie = imie
		self.nazwisko = nazwisko
		self.plec = plec
		self.wiek = wiek
		self.wzrost = wzrost

	def Przedstaw(self):
		print("Jestem " + self.imie + " " + self.nazwisko)

	def Wiek(self):
		print(self.wiek)

	def Wyzszy(self,ile):
		if self.wzrost < ile:
			print("Jestes wyzszy!")
		elif self.wzrost > ile:
			print("Jestes nizszy!")
		else:
			print("Jestesmy rowni!")

def main():
	A = Czlowiek("Kasia","Azorkiewicz","kobieta",21,180)
	Czlowiek.Przedstaw(A)
	Czlowiek.Wiek(A)
	Czlowiek.Wyzszy(A,190)
main()
