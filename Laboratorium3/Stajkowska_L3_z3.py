import sys
	
def main():
	if len(sys.argv)==1:
		print("Błędne urzycie programu !")
		sys.exit(1)
	try:
		source = open(sys.argv[1],"rb")
		aim = open(sys.argv[2],"wb")
	except:
		print("Nie można otworzyć conajmniej jednego z plików podanych jako argumenty !")
		sys.exit(2)

	while True:
		buf = source.read(1)  
		if len(buf) == 0:
			break
		aim.write(buf)
		aim.write(buf)

	source.close()
	aim.close()

main()
