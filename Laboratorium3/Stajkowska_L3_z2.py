import sys

def main():
	if len(sys.argv)==1:
		print("Błędne urzycie programu !")
		sys.exit(1)
	try:
		plik = open(sys.argv[1],"r")
	except:
		print("Nie można otworzyć pliku podanego jako argument !")
		sys.exit(2)
	ile=0
	lista=plik.readlines()
	seek=int(sys.argv[2])
	for i in lista:
		for j in i:
			if(ord(j)==seek):
				ile+=1
	print("W twoim pliku, bajt o nr."+sys.argv[2]+" występuje "+str(ile)+" razy.")
main()
