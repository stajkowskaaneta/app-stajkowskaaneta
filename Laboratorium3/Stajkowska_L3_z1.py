import sys

def main():
	l=len(sys.argv)
	if l==1:
		print("Bład!")
		sys.exit(1)
	for i in range(1,l):
		try:
			plik = open(sys.argv[i],"r")
		except:
			print("Nie można otworzyć jednego z plików podanych jako argumenty !")
			sys.exit(2)
		print("*"*64)
		print(sys.argv[i])
		print("*"*64)
		linia=plik.readline()
		while(linia!=""):
			print(linia)
			linia=plik.readline()	
		plik.close()
main()
	
