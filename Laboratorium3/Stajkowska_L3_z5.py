import sys

def main():
	counter=0
	try:
		plik = open(sys.argv[1],"rb")
	except:
		print("Nie można otworzyć pliku podanego jako argument !")
		sys.exit(1)
	while True:
		buf = plik.read(1024)  
		if len(buf) == 0:
			break
		for i in buf:
			counter+=1
			if counter%64==0:
				print("\n")
			else:
				print(i,end=" ")
				
	plik.close()
	
	
main()
