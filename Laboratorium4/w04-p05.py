import os

def main():
    nazwa_katalogu = "test_directory"

    print("Tworzymy katalog", nazwa_katalogu)
    os.makedirs(nazwa_katalogu)

    nazwa_pliku = os.path.join(nazwa_katalogu, "test_file.txt")
    print("Tworzymy plik", nazwa_pliku)
    f = open(nazwa_pliku, "wt")
    try:
        f.write("Python. Programming is fun again")
    finally:
        f.close()

    print("Zawartość katalogu", nazwa_katalogu)
    print(os.listdir(nazwa_katalogu))

    print("Usuwamy plik", nazwa_pliku)
    os.unlink(nazwa_pliku)
    
    print("Zawartość katalogu", nazwa_katalogu)
    print(os.listdir(nazwa_katalogu))
    
    print("Usuwamy katalog", nazwa_katalogu)
    os.rmdir(nazwa_katalogu)

if __name__ == "__main__":
    main()

