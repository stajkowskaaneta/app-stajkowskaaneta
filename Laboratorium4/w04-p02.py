import os
import sys


def main():
    l = len(sys.argv)
    if l == 1:
        print("Malo argumentow")
        sys.exit(1)
    for i in range(1,l):
        __file__ = sys.argv[i]
        print("Testowanie pliku", __file__)
        print("Plik istnieje:", os.access(__file__, os.F_OK))
        print("Plik można czytać:", os.access(__file__, os.R_OK))
        print("Plik jest zapisywalny:", os.access(__file__, os.W_OK))
        print("Plik jest wykonywalny:", os.access(__file__, os.X_OK))
        print(" ")
    
if __name__ == "__main__":
    main()

