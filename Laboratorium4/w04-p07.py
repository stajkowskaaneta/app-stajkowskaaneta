import os, sys

# Jeżeli nie podano katalogu, to używamy katalogu /tmp
if len(sys.argv) == 1:
    root = "/tmp"
else:
    root = sys.argv[1]

for katalog, podkatalogi, pliki in os.walk(root):
    print("\n", katalog)
    # Dodajemy znak / na końcu nazwy każdego podkatalogu
    print(podkatalogi)
    podkatalogi = [ "{0}/".format(katalog) for katalog in podkatalogi ]
    print(podkatalogi)
    
    zawartość = podkatalogi + pliki
    zawartość.sort()
    
    # Pokaż zawartość
    for element in zawartość:
        print("\t{0}".format(element))
    print(zawartość)
