import os
import stat

def main():
    nazwa_pliku = "plik_testowy.txt"
    if os.path.exists(nazwa_pliku):
        # os.unlink(nazwa_pliku)
        pass
    else:
        f = open(nazwa_pliku, "wt")
        f.write("zawartość")
        f.close()

    # Określamy istniejące uprawnienia używajac stat
    uprawnienia = stat.S_IMODE(os.stat(nazwa_pliku).st_mode)

    if not os.access(nazwa_pliku, os.X_OK):
        print("Dodajemy prawo do wykonywania")
        # używamy bitowej alternatywy aby ustawić prawo do wykonywania przez użytkownika
        nowe_uprawnienia = uprawnienia | stat.S_IXUSR
    else:
        print("Zabieramy prawo do wykonywania")
        # używamy bitowej alternatywy rozłącznej aby usunąć prawo do wykonywania przez użytkownika
        nowe_uprawnienia = uprawnienia ^ stat.S_IXUSR

    os.chmod(nazwa_pliku, nowe_uprawnienia)

if __name__ == "__main__":
    main()

