import os
import stat

def main():
	print("Upewnij sie, ze nie istnieje juz folder PoryRoku")
	adres = "PoryRoku/"
	foldery = ["wiosna/","lato/","jesien/","zima/"]
	miech = ["marzec/", "kwiecien/","maj/","czerwiec/","lipiec/","sierpien/","wrzesien/","pazdziernik/","listopad/","grudzien/","styczen/","luty/"]
	l = 0
	for i in foldery:
		for y in range(l,l+3):
			suma = adres + i + miech[y]
			os.makedirs(suma)
			l += 1
	nazwa = "PoryRoku"
	upraw = stat.S_IMODE(os.stat(nazwa).st_mode)
	grupy = [upraw ^ stat.S_IXGRP,upraw ^ stat.S_IWGRP,upraw ^ stat.S_IRGRP,upraw ^ stat.S_IROTH,upraw ^ stat.S_IWOTH, upraw ^stat.S_IXOTH]
	for i in grupy:
		os.chmod(nazwa,i)

main()
