import os

def main():
    print("Katalog bieżący:", os.getcwd())

    print("Katalog nadrzędny dla biężącego katalogu:", os.pardir)
    print("Przechodzimy do katalogu", os.pardir)
    os.chdir(os.pardir) # Przechodzimy do katalogu nadrzędnego

    print("Katalog bieżący po zmianie:", os.getcwd())

if __name__ == "__main__":
    main()

