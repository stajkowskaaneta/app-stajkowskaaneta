import os, tempfile

def main():
    nazwa_dowiazania = tempfile.mktemp()

    print("Tworzymy dowiązanie {0} -> {1}".format(nazwa_dowiazania, __file__))
    os.symlink(__file__, nazwa_dowiazania)

    stat_info = os.lstat(nazwa_dowiazania)
    print("Prawa dostępu:", oct(stat_info.st_mode))

    print("Dowiązanie wskazuje na:", os.readlink(nazwa_dowiazania))

    print("Usuwamy dowiązanie")
    os.unlink(nazwa_dowiazania)

if __name__ == "__main__":
    main()

