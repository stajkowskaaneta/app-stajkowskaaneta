import os
import sys
import time

def main():
    # Jeżeli nie podano pliku, to używamy pliku żródłowego niniejszego programu
    if len(sys.argv) == 1:
        nazwa_pliku = __file__
    else:
        nazwa_pliku = sys.argv[1]

    for i in range(1,len(sys.argv)):
             nazwa_pliku = sys.argv[i]
             stat_info = os.stat(nazwa_pliku)
             print("os.stat({0}):".format(nazwa_pliku))
             print("\tRozmiar:", stat_info.st_size)
             print("\tUprawnienia:", oct(stat_info.st_mode))
             print("\tWłaściciel: użytkownik o numerze uid =", stat_info.st_uid)
             print("\tUrządzenie:", stat_info.st_dev)
             print("\tOstatnio modyfikowany:", time.ctime(stat_info.st_mtime))
             print(" ")

if __name__ == "__main__":
    main()

