import os.path

def main():
    """ Wybrane funkcje modułu os.path
    """

    """ os.path.split -- dzieli nazwę ścieżki na parę, (głowa, ogon), 
    gdzie ogon jest ostatnia częścią ścieżki, a głowa jest fragmentem
    bez ogona """
    print(os.path.split(os.getcwd()))

    print(os.path.splitext("app-w02.tar.bz2"))

    print(os.path.abspath('.'))
    print(os.path.abspath('..'))

    print(os.path.basename("home/andrzej/.bashrc"))

    print(os.path.commonprefix(["/home", "/home/andrzej"]))
    print(os.path.commonprefix(["/home", "/home/andrzej", "/"]))
    print(os.path.commonprefix(["/home", "/home/andrzej", ""]))

    print(os.path.realpath("/etc/passwd"))
    print(os.path.realpath("/bin/cp"))

    print(os.path.isdir("/"))

    print(os.path.exists("/"))

    print(os.path.dirname("/usr/bin/cp"))

    print(os.path.normpath("/usr//bin/cp"))

    print(os.path.expandvars("${PATH}"))
    print(os.path.expandvars("${SHELL}"))

    print(os.path.expanduser("~"))

    print(os.path.ismount("/mnt"))
    print(os.path.ismount("/home"))
    print(os.path.ismount("/"))

    print(os.path.getsize("/etc/passwd"))

    print(os.path.isabs("."))
    print(os.path.isabs("/usr"))

    print(os.path.islink("/bin/cp"))
    print(os.path.islink("/usr/bin/cp"))

    print(os.path.relpath("/home/andrzej"))
    print(os.path.relpath("/home/andrzej/Pobrane"))

    print(os.path.samefile("/bin/cp", "/usr/bin/cp"))

if __name__ == "__main__":
    main()

