s = False

def potega(q=9,w=6,e=2017):
	d=q
	m=w
	r=e
	miesiace = [1,3,5,7,8,10,12]
	drugie = [2,4,6,9,11]
	while True:
		yield d, ":",m,":",r
		if m in miesiace:
			if m != 12:
				if d == 31:
					m += 1
					d = 1
				else:
					d += 1
			else:
				if d == 31:
					m = 1
					d = 1
					r += 1
				else:
					d += 1
		else:
			if m != 2:
				if d == 30:
					m += 1
					d = 1
				else:
					d += 1
			else:
				if r % 4 == 0:
					if d == 29:
						m += 1
						d = 1
				else:
					if d == 28:
						m += 1
						d = 1
				if d < 28:
					d += 1

def main():
	c = potega()
	for i in range(20):
		print(next(c))	
main()
