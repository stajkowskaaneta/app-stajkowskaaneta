def fib(s=0):
    p = s
    d = p + 1
    while True:
        yield p
        temp = p + d
        d = p
        p = temp

def main():
    c = fib()
    for i in range(20):
         print(next(c))

main()
