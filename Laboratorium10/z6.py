def potega(s=2):
	n = s
	while True:
		ile = 0
		i = 2
		while (i <= n):
			if n % i == 0:
				ile += 1
			i = i + 1
		if ile == 1:
			yield n
		n = n + 1 

def main():
    c = potega()
    for i in range(20):
        print(next(c))

main()
