from z10 import z1
from z10 import z2
from z10 import z6

def main():
	print("Zadanie pierwsze")
	c = z1()
	d = z1.generator(c)
	for i in range(5):
		print(next(d))

	print("Zadanie drugie")
	c = z2()
	d = z2.generator(c)
	for i in range(5):
		print(next(d))

	print("Zadanie trzecie")
	c = z6()
	d = z6.generator(c)
	for i in range(5):
		print(next(d))
main()
