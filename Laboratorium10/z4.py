def potega(s=1):
    n = s
    z = 1
    while True:
        yield z
        z *= n
        n += 1

def main():
    c = potega()
    for i in range(20):
        print(next(c))

main()
