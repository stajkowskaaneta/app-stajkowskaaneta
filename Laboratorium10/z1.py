
def potega(s=0):
    n = s
    while True:
        yield 2**n
        n += 1

def main():
    c = potega()
    for i in range(20):
        print(next(c))

main()
