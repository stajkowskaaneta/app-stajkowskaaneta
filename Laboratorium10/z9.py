def potega(s=2):
	n = s
	while True:
		ile = 0
		for i in range(1,n):
			if n % i == 0:
				ile += i
		if ile == n:
			yield n
		n = n + 1 

def main():
    c = potega()
    for i in range(3):
        print(next(c))

main()
