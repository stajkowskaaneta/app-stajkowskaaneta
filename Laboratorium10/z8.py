
def potega(q=23,w=58):
	g = q
	m = w
	while True:
		yield g, ":", m
		if m != 59:
			m += 1
		else:
			m = 0
			if g == 23:
				g = 0
			else:
				g += 1		

def main():
	c = potega()
	for i in range(20):
		print(next(c))

main()
