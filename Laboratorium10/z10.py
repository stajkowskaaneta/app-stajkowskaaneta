
class z1():
	def __init__(self,s=0):
		self.__n = s
	
	def generator(self):
		while True:
			yield 2**self.__n
			self.__n += 1

class z2():
	def __init__(self,s=0):
		self.__p = s
		self.__d = self.__p + 1

	def generator(self):
		while True:
			yield self.__p
			temp = self.__p + self.__d
			self.__d = self.__p
			self.__p = temp


class z6():
	def __init__(self,s=2):
		self.__n = s

	def generator(self):
		while True:
			ile = 0
			i = 2
			while ( i <= self.__n):
				if self.__n % i == 0:
					ile += 1
				i = i + 1
			if ile == 1:
				yield self.__n
			self.__n = self.__n + 1
