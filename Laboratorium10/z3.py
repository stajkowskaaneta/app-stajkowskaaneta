
def potega(s=0):
    n = s
    while True:
        yield n
        n += 1

def main():
    c = potega()
    suma = 0
    for i in range(20+1):
        b = (next(c))
        suma += b
        print(b)
    print("SUMA: ", suma)

main()
