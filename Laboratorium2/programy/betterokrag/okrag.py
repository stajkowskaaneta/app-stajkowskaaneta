from math import pi

class Okrag:
    def __init__(self, promien):
        self.__promien = float(promien)
    
    @property
    def promien(self):
        return self.__promien

    @promien.setter
    def promien(self, val):
        try: 
            self.__promien = float(val)
        except: 
            raise TypeError("Argument musi być liczbą!")

    @promien.deleter
    def promien(self):
        raise TypeError("Nie można usunąć pola promien")

    @property
    def pole(self):
        return pi * self.__promien ** 2
    
    @property
    def obwod(self):
        return 2 * pi * self.__promien
