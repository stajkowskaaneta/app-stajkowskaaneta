from punkt import Punkt

def main():
    a = Punkt(5, 8)
    print("Utworzony punkt:", a)
    a.move(-2, 3)
    print("Punkt po przesunięciu: ", a)
    print("Typ utworzonego obiektu:", type(a))
    print("Słownik __dict__ dla utworzonego obiektu:")
    print(a.__dict__)
    del a.x
    print(a.__dict__)

if __name__ == "__main__":
    main()
