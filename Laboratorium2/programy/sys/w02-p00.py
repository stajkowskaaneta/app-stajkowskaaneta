#!/usr/bin/env python3

import sys

def main():
    print(sys.byteorder)
    print()
    print(sys.copyright)
    print()
    print(sys.float_info)
    print()
    print(sys.int_info)
    print()
    print(sys.modules)
    print()
    print(sys.path)
    print()
    print(sys.platform)
    print()
    print(sys.prefix)
    # print(sys.ps1)    # tylko w interpreterze
    # print(sys.ps2)    # tylko w interpreterze
    
    print('Default encoding     :', sys.getdefaultencoding())
    print('File system encoding :', sys.getfilesystemencoding())

if __name__ == "__main__":
    main()
