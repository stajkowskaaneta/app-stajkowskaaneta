#!/usr/bin/env python3

import sys

def main():
    for a in sys.argv:
        print(a, end = " ")
    print()

if __name__ == "__main__":
    main()
