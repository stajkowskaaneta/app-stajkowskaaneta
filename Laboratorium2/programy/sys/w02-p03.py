#!/usr/bin/env python3

import sys

def main():
    for j in range(len(sys.argv)):
        print(str(j) + ". " + sys.argv[j])
        print(type(sys.argv[j]))

if __name__ == "__main__":
    main()
