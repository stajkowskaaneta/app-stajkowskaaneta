from math import pi

class Okrag:
    def __init__(self, promien):
        self.promien = float(promien)
    
    @property
    def pole(self):
        return pi * self.promien ** 2
    
    @property
    def obwod(self):
        return 2 * pi * self.promien
