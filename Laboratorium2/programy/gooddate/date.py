import time

class Date(object):
    def __init__(self, y, m, d):
        self.y, self.m, self.d = y, m, d
        
    @classmethod
    def now(cls):
        t = time.localtime()
        return cls(t.tm_year, t.tm_mon, t.tm_mday)
        
    @classmethod
    def tomorrow(cls):
        t = time.localtime(time.time() + 24 * 60 * 60)
        return cls(t.tm_year, t.tm_mon, t.tm_mday)
