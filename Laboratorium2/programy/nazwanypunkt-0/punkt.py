class Punkt:
    ile = 0

    def __init__(self, x, y):
        self.x, self.y = x, y
        Punkt.ile += 1

    def move(self, deltaX, deltaY):
        self.x += deltaX
        self.y += deltaY
    
    def __str__(self):
        return ("<" + str(self.x) + ", "
            + str(self.y) + ">")
