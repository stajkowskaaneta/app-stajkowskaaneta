from punkt import Punkt

class NazwanyPunkt(Punkt):
    ile = 0

    def __init__(self, x, y, nazwa):
        Punkt.__init__(self, x, y)
        self.nazwa = nazwa
        NazwanyPunkt.ile + 1

    def __str__(self):
        return (self.nazwa + "<" + str(self.x) 
            + ", " + str(self.y) + ">")
      
