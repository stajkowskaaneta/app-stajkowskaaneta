import time

class Date(object):
    def __init__(self, y, m, d):
        self.y, self.m, self.d = y, m, d
        
    @staticmethod
    def now():
        t = time.localtime()
        return Date(t.tm_year, t.tm_mon, t.tm_mday)
        
    @staticmethod
    def tomorrow():
        t = time.localtime(time.time() + 24 * 60 * 60)
        return Date(t.tm_year, t.tm_mon, t.tm_mday)
