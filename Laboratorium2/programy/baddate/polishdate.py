from date import Date

class PolishDate(Date):
    def __str__(self):
        s = "{:04}-{:02}-{:02}"
        return s.format(self.y, self.m, self.d)
