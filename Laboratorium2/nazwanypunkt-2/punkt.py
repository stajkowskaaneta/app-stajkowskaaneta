from math import sqrt

class Punkt:
	ile = 0

	def __init__(self, x, y):
		self.__x, self.__y = float(x), float(y)
		Punkt.ile += 1

	def move(self, deltaX, deltaY):
		self.__x += deltaX
		self.__y += deltaY

	@property
	def x(self):
		return self.__x

	@property
	def y(self):
		return self.__y

	@property
	def length(self):
		try:
			return sqrt((self.__x**2+self.__y**2))
		except:
			raise ValueError ("Odmawiam wykonania działania, coś musiałeś zepsuć!")

	def distance(self, x0, y0):
		self.__x0, self.__y0 = x0, y0
		return sqrt((self.__x0-self.__x)**2 + (self.__y0-self.__y)**2)

	@property
	def __str__(self):
		return ("<" + str(self.x) + ", " + str(self.y) + ">")

	@x.deleter
	def x(self):
		raise PermissionError("Nie masz uprawnień usuwać tej danej!")
	@y.deleter
	def y(self):
		raise PermissionError("Nie masz uprawnień usuwać tej danej!")

#	def length(self):
#		raise PermissionError ("Nie masz uprawnień usuwać długości odcinka!")
