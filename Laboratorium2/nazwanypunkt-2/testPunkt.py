from punkt import Punkt
from nazwanypunkt import NazwanyPunkt
from random import randint as ri

def main():
	a = NazwanyPunkt(5, 8, "tawerna")
	print(a)
	a.move(-2, 3)
	print(a)
	print(a.length)
	rand_x, rand_y=ri(0,10),ri(0,10)
	print ("Randomowe wartości wylosowane to:",rand_x,"oraz", rand_y)
	print("Odległość pomiędzy nadanym a wylosowanym:",a.distance(rand_x, rand_y))
	b = Punkt(3, 4)
	try:
		del b.x
	except:
		print ("Nie można usunąć!")


if __name__ == "__main__":
	main()
