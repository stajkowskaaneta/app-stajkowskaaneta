class Punkt:
    ile = 0

    def __init__(self, x, y):
        self.__x, self.__y = x, y
        Punkt.ile += 1

    def __del__(self):
        Punkt.ile -= 1

    def getIle():
        return ile

    def move(self, deltaX, deltaY):
        self.__x += deltaX
        self.__y += deltaY

    def distance(self, other):
        return ((self.x - other.x) ** 2 + (self.y - other.y) ** 2) ** 0.5

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def length(self):
        return (self.__x ** 2 + self.__y ** 2) ** 0.5

    def __str__(self):
        return ("<" + str(self.x) + ", " + str(self.y) + ">")

    @x.deleter
    def x(self):
        raise PermissionError("Nie mozna usunac!")
    @y.deleter
    def y(self):
        raise PermissionError("Nie mozna usunac!")
