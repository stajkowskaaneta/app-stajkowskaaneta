from punkt import Punkt

def main():
    b = Punkt(3, 4)
    print(b)
    b.move(2, 2)
    print(b)
    c = Punkt(3,4).length()
    print(c)
    d = Punkt(0, 0)
    e = Punkt(3, 4)
    f = d.distance(e)
    print(f)


if __name__ == "__main__":
    main()
