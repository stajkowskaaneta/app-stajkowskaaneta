from punkt import Punkt

def main():
    b = Punkt(3, 4)
    print(b)
    d = Punkt(1, 1)
    e = Punkt(3, 4)
    
    g = e.__add__(b)
    h = e.__iadd__(b)
    print(g)
    print(h)
    print(e.__sub__(b))
    print(e.__isub__(b))
    print(e.__mul__(b))
    print(e.__imul__(b))
    print(e.__truediv__(b))
    print(e.__itruediv__(b))
    print(e.__floordiv__(b))
    print(e.__ifloordiv__(b))

if __name__ == "__main__":
    main()
