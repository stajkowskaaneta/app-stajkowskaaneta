from punkt import Punkt
from nazwanypunkt import NazwanyPunkt

def main():
    a = NazwanyPunkt(5, 8, "tawerna")
    print(a)
    a.move(-2, 3)
    print(a)
    print(a.__dict__)
    b = Punkt(3, 4)
    print(b)
    x = Punkt(-2, 0)
    y = NazwanyPunkt(6, 18, "karawan")

    c = b.ile
    print(c)
    d = a.ile
    print(d)

if __name__ == "__main__":
    main()
