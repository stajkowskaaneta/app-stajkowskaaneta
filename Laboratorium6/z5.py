from graphics import *
def main():
    win = GraphWin("Kwadraty", 600, 600)
    shape = Rectangle(Point(30, 30), Point(20, 20))
    shape.setOutline("red")
    shape.setFill("red")
    shape.draw(win)
    for j in range(2):
        p = win.getMouse()
        c = shape.getCenter()
        dx = p.getX() - c.getX()
        dy = p.getY() - c.getY()
        shape = Rectangle(Point(p.getX(), p.getY()), Point(p.getX() - 10, p.getY() - 10))
        shape.setOutline("red")
        shape.setFill("red")
        shape.draw(win)
    ex = GraphWin("Czy zakonczyc?", 210, 80)
    e = Text(Point(100, 25), "Kliknij, by zakończyć")
    e.draw(ex)
    x = ex.getMouse()
    ex.close()
    win.close()

main()
