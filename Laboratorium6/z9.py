
from graphics import *
import random

def main():
	okno = GraphWin("Linie", 800, 900)
	for i in range(5):
		x = Point(random.randint(0,800),random.randint(0,900))
		y = Point(random.randint(0,800),random.randint(0,900))
		l = Line(x,y)
		l.draw(okno)
	okno.getMouse()	
	okno.close()

main()
