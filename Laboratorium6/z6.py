from graphics import *
def main():
    height = 600
    width = 600
    win = GraphWin("Tarcza", height, width)
    radius = 50
    
    shape = Rectangle(Point(0, 0), Point(height, width))
    shape.draw(win)
    
    shape = Circle(Point(width/2, height/2), radius + 4*(radius/2))
    shape.setOutline("black")
    shape.setFill("white")
    shape.draw(win)
    
    shape = Circle(Point(width/2, height/2), radius + 3*(radius/2) )
    shape.setOutline("black")
    shape.setFill("black")
    shape.draw(win)
    
    shape = Circle(Point(width/2, height/2), radius + radius/2 + radius/2)
    shape.setOutline("black")
    shape.setFill("blue")
    shape.draw(win)
    
    shape = Circle(Point(width/2, height/2), radius + radius/2)
    shape.setOutline("black")
    shape.setFill("red")
    shape.draw(win)
    
    shape = Circle(Point(width/2, height/2), radius)
    shape.setOutline("black")
    shape.setFill("yellow")
    shape.draw(win)
    
    win.getMouse()
    win.close()
    

main()
