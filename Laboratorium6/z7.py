from graphics import *

def main():
    width = 600
    height = 600
    win = GraphWin("Klikaj, by narysować twarz!", width, height)
    win.getMouse()
    
    shape = Circle(Point(width/2, height/2), height/2)
    shape.draw(win)
    win.getMouse()
    
    shape = Circle(Point(width/3, height/3), height/10)
    shape.draw(win)
    win.getMouse()
    
    shape = Circle(Point(width/2 + width/10, height/3), height/10)
    shape.draw(win)
    win.getMouse()
    
    shape = Circle(Point(width/3, height/3), height/20)
    shape.draw(win)
    win.getMouse()
    
    shape = Circle(Point(width/2 + width/10, height/3), height/20)
    shape.draw(win)
    win.getMouse()
    
    x = Point(width/2, height/2)
    y = Point(width/2 + 50, height/2 + 50)
    shape = Line(x, y)
    shape.draw(win)
    
    shape = Line(y,  Point(width/2, height/2 - 50))
    shape.draw(win)
    win.getMouse()
    
    shape = Rectangle(Point(width/2 - width/6, height/2 + height/3), Point(width/2 + width/6, height/2 + height/5))
    shape.draw(win)

    win.getMouse()
    win.close()
    
    
main()
