from graphics import *
from math import sqrt

def main():
    width = 600
    height = 600
    win = GraphWin("Kości!", width, height)
    p = win.getMouse()
    r = win.getMouse()
    p1 = Point(p.getX(), p.getY())
    p2 = Point(r.getX(), r.getY())
    line = Rectangle(p1, p2)
    line.draw(win)
    
    obwod = abs(2*(p.getX() - r.getX()) + 2*(p.getY()- r.getY()))
    win2 = GraphWin("Wyniki", width/2, height/2)
    pole = abs((p.getX() - r.getX()) * (p.getY()- r.getY()))
    
    t = Text(Point(width/5, height/5), "Obwod = {0}\n Pole = {1}".format(obwod, pole))

    t.draw(win2);
    win2.getMouse()
    win2.close()
    win.close()
    


main()
