from graphics import *
from math import sqrt

def main():
    width = 600
    height = 600
    win = GraphWin("Kości!", width, height)
    p = win.getMouse()
    r = win.getMouse()
    s = win.getMouse()
    p1 = Point(p.getX(), p.getY())
    p2 = Point(r.getX(), r.getY())
    p3 = Point(s.getX(), s.getY())
    line = Polygon(p1, p2, p3)
    line.draw(win)
    
    px = p.getX()
    py = p.getY()
    
    rx = r.getX() 
    ry = r.getY()
    
    sx = s.getX()
    sy = s.getY()
    
    pr = sqrt((rx - px)**2 + (ry - py)**2) # a 
    rs = sqrt((sx - rx)**2 + (sy - ry)**2) # b
    ps = sqrt((sx - px)**2 + (sy - py)**2) # c
    
    a = pr
    b = rs
    c = ps
    
    p = 1/2*(a + b + c)
    
    obwod = abs(pr + rs + ps)
    pole = sqrt(p*(p-a)*(p-b)*(p-c))
    
    win2 = GraphWin("Wyniki", width/2, height/2)
    t = Text(Point(width/5, height/5), "Obwod = {0}\n Pole = {1}".format(obwod, pole))

    t.draw(win2);
    win2.getMouse()
    win2.close()
    win.close()
    


main()

