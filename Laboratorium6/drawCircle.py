from graphics import GraphWin, Circle, Point

def main():
    win = GraphWin("Okrąg", 800, 600)
    c = Circle(Point(400, 300), 200)
    c.draw(win)
    print(c.getCenter().getX(), c.getCenter().getY())
    print(c.getRadius())
    win.getMouse()
    win.close()

if __name__ == "__main__":
    main()

