
from graphics import *
import random
import math

def main():
	w = 800
	h = 900
	okno = GraphWin("Uklad", w, h)
	
	okno.setCoords(-(w/2),-(h/2),(w/2),h/2)
	x = Point(-300,0)
	y = Point(300,0)
	l = Line(x,y)
	l.draw(okno)
	
	x = Point(0,400)
	y = Point(0,-400)
	l = Line(x,y)
	l.draw(okno)
	
	x = 0	
	for i in range(2000):
		y = 30 * math.sin(x/30)
		p = Point(x,y)
		p.draw(okno)
		x += 1


	okno.getMouse()	
	okno.close()

main()
