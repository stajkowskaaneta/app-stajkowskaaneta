from graphics import GraphWin, Point, Line

""" Rysuje układ współrzędnych """
def main():
    win = GraphWin("Układ współrzędnych", 800, 600)
    win.setCoords(-400, -300, 400, 300)
    x = Line(Point(-395, 0), Point(395, 0))
    x.setWidth(2); x.draw(win); x.setArrow("last")
    y = Line(Point(0, -295), Point(0, 295))
    y.setWidth(2); y.draw(win); y.setArrow("last")
    win.getMouse(); win.close()

if __name__ == "__main__":
    main()

