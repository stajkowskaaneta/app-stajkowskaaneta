from graphics import *
from math import sqrt

def main():
    width = 600
    height = 600
    win = GraphWin("Kości!", width, height)
    p = win.getMouse()
    r = win.getMouse()
    p1 = Point(p.getX(), p.getY())
    p2 = Point(r.getX(), r.getY())
    line = Rectangle(p1, p2)
    line.draw(win)
    
    trzecie = win.getMouse()

    szerokosc = 1/10*abs(p.getX() - r.getX())
    
    line = Rectangle(Point(trzecie.getX() - szerokosc, trzecie.getY()), Point(trzecie.getX() + szerokosc, r.getY()))
    line.draw(win)
    
    czwarte = win.getMouse()
    line = Rectangle(Point(czwarte.getX() + szerokosc/2, czwarte.getY() + szerokosc/2), Point(czwarte.getX() - szerokosc/2, czwarte.getY() - szerokosc/2))
    line.draw(win)
    
    piate = win.getMouse()
    line = Polygon(Point(piate.getX(), piate.getY()), Point(p.getX(), p.getY()), Point(r.getX(), p.getY()))
    line.draw(win)
    
    win.getMouse()
    
main()
