from graphics import GraphWin, Point, Rectangle

def main():
    win = GraphWin("Prostokąt", 300, 300)
    r = Rectangle(Point(10, 10), Point(100, 100))
    r.draw(win)
    print(r.getP1().getX(), r.getP1().getY())
    print(r.getP2().getX(), r.getP2().getY())
    win.getMouse()
    win.close()

if __name__ == "__main__":
    main()

