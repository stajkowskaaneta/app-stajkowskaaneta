from graphics import *
from math import sqrt

def main():
    width = 600
    height = 600
    win = GraphWin("Kości!", width, height)
    p = win.getMouse()
    r = win.getMouse()
    p1 = Point(p.getX(), p.getY())
    p2 = Point(r.getX(), r.getY())
    line = Line(p1, p2)
    line.draw(win)
    srodek = Point((p.getX()+r.getX())/2, (p.getY()+r.getY())/2)
    srodek.setOutline("cyan")
    srodek.setFill("cyan")
    srodek.draw(win)
    x1 = p.getX()
    y1 = p.getY()
    x2 = r.getX()
    y2 = r.getY()
    
    win2 = GraphWin("Wyniki", width/2, height/2)
    dlugosc = sqrt((x2-x1)**2+(y2-y1)**2)

    tg = srodek.getY()/srodek.getX()
    t = Text(Point(width/5, height/5), "Dlugosc = {0}\n tangens = {1}".format(dlugosc, tg))

    t.draw(win2);
    win2.getMouse()
    win2.close()
    win.close()
    
main()
