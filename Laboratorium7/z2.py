class RejestrowanieKasy:
	
	def __init__(self):
		self.itemCount = 0
		self.totalPrice = 0
        
	def addItem(self,arg):
		self.itemCount += 1
		self.totalPrice += arg
        	
	def getTotal(self):
		return self.totalPrice
        
	def getCount(self):
		return self.itemCount
        
	def clear(self):
		self.itemCount = 0
		self.totalPrice = 0 

def main():
	kasa = RejestrowanieKasy()
	print("{0}  {1}".format(kasa.getTotal(),kasa.getCount()))
    
	kasa.addItem(10)
	print("{0}  {1}".format(kasa.getTotal(),kasa.getCount()))
    
	kasa.addItem(20)
	print("{0}  {1}".format(kasa.getTotal(),kasa.getCount()))
    
	kasa.addItem(20)
	print("{0}  {1}".format(kasa.getTotal(),kasa.getCount()))
    
	kasa.clear()
	print("{0}  {1}".format(kasa.getTotal(),kasa.getCount()))
main()
