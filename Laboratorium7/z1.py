class Menu():
	
	def __init__(self):
		self.l=[]
        
	def addOption(self,opcja):
		self.l+=[opcja]
        
	def getInput(self):
		n=len(self.l)
		for i in range(n):
			print("["+str(i)+"] "+self.l[i])
		while True:
			a=input("Podaj opcję: ")
			try:
				a=int(a)
				if a>n or a==self.l.index("Wyjście"):
					break						
				else:
					print("Input ["+str(a)+"]")
			except:
				if a=="Wyjście":
					break
				elif a not in self.l:
					for i in range(n):
						print("["+str(i)+"] "+self.l[i])
				else:
					print("Input ["+str(self.l.index(a))+"]")
					
def main():
	menu=Menu()
    
	menu.addOption("Otwórz nowe konto")
	menu.addOption("Zaloguj do istniejącego konta")
	menu.addOption("Pomoc")
	menu.addOption("Wyjście")
    
	menu.getInput()
    
main()
		
