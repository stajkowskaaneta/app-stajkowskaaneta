#Stajkowska Aneta

def main():
	macierz_a=[[u for u in range(i,3+i)] for i in range(1, 11, 3)]
	macierz_b=[[u for u in range(i,i-6,-2)] for i in range(8, -2, -4)]
	suma(macierz_a,macierz_b)

def suma(a,b):
	c=[]
	print ("Dodawanie macierzy!\n{0:9} + {1:11} = {2:11}".format("Macierz A", "Macierz B", "Macierz C"))
	for i in range(len(a)-1):
		temp=[]
		for j in range(len(a[i])):
			zlicz=a[i][j]+b[i][j]
			temp.append(zlicz)
		c.append(temp)
		print("{0:9} + {1:11} = {2:11}".format(str(a[i]), str(b[i]), str(c[i])))
	return c


main()
