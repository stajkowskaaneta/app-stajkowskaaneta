#!/usr/bin/python3

import _thread
import time
import sys

class Zegar:
	def nastaw(self, godzina, minuta, sekunda):
		self.godzina = godzina
		self.minuta = minuta
		self.sekunda = sekunda
		if self.godzina >= 24 or self.godzina < 0 or self.minuta > 60 or self.minuta < 0 or self.sekunda > 60 or self.sekunda < 0:
			print("Bledne dane")
			sys.exit()
		
	def wypisz(self):
		print("%s:%s:%s" % (self.godzina, self.minuta, self.sekunda))
		
	def format(self, form):
		self.form = form
		if self.form == 12:
			if self.godzina > 12:
				self.godzina -= 12
		if self.form == 24 and self.godzina <= 12 and self.godzina >= 0:
			self.z = input("Czy godzina jest w formacie AM czy PM? ")
			if self.z == "AM" or self.z == "am":
				pass
			elif (self.godzina + 12) < 24 and self.z == "PM" or self.z == "pm":
				self.godzina += 12
		else:
			pass
			
	def tykniecie(self):
		self.sekunda += 1
		if self.sekunda == 60:
			self.godzina += 1
			self.sekunda = 0
		if self.godzina == 24:
			self.godzina = 0
		

def main():
	zegarek = Zegar()
	zegarek.nastaw(11, 12, 30)
	zegarek.wypisz()
	zegarek.tykniecie()
	zegarek.format(24)
	zegarek.wypisz()
	
	
	
main()
