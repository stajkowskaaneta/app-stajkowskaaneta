#!/usr/bin/python3

import _thread
import time

def main():
    try:
        x = int(input("Podaj ilosc minut: "))
        y = int(input("Podaj ilosc sekund: "))
        _thread.start_new_thread(pokazCzas, (1, x, y ) )

    except:
        print ("Error: unable to start thread")

    while 1:
        pass

def pokazCzas(delay, x, y):

    x = (x * 60) + y
    while x:
        time.sleep(delay)
        print("minut: %s, sekund: %s" % (x//60, x%60))
        x -= 1

main()

