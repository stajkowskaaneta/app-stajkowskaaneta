#!/usr/bin/python3

import _thread
import time
from tkinter import *
from tkinter import font

class Application(Frame):

    def __init__(self, master):
        super().__init__(master)  
        self.grid()
        self.create_widgets()

    def create_widgets(self):

        self.pw_lbl = Label(self, text = "Ile minut: ")
        self.pw_lbl.grid(row = 0, column = 0, columnspan = 2, sticky = W)
              
        self.pw_ent = Entry(self)
        self.pw_ent.grid(row = 0, column = 1, sticky = W)
        
        self.pw_lbl1 = Label(self, text = "Ile sekund: ")
        self.pw_lbl1.grid(row = 1, column = 0, sticky = W)
        
        self.pw_ent1 = Entry(self)
        self.pw_ent1.grid(row = 1, column = 1, sticky = W)
        
        self.secret_txt = Text(self, width = 35, height = 5, wrap = WORD)
        self.secret_txt.grid(row = 3, column = 0, columnspan = 2, sticky = W)
        
        self.submit_bttn = Button(self, text = "Zacznij", command = lambda : self.pokazCzas(1))
        self.submit_bttn.grid(row = 2, column = 0, sticky = W)


        

    
    def pokazCzas(self, delay = 1):
        a = int(self.pw_ent.get())
        b = int(self.pw_ent1.get())
        d = (a * 60) + b
        delay = int(delay)
        while d>0:         
            self.secret_txt.insert(0.0, "minut: " + str(d//60) + " sekund: " + str(d%60) + "\n")
            d -= 1
            self.master.update_idletasks()
            time.sleep(delay)
        self.secret_txt.insert(0.0, "KONIEC!!\n")

        
def main():

    root = Tk()
    app = Application(root)
    root.mainloop()



main()


